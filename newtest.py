#Python program to create an Employee Database

import os
import uuid
import xml.etree.cElementTree as ET


class MY_DB_FILE:
	prefix = "C:\\Users\\nsurisetti\\Desktop"
	suffix = ".xml"
	id = uuid.uuid1()

	def __init__(self,file_name):
		
		# if not isinstance(file_name,str):
		if not isinstance(file_name,str):
			raise TypeError("Expected string argument, but recieved inappropriate type")

		if file_name.isalpha() == False:
			raise ValueError("Expected a valid alphabetic string but received inappropriate value")

		dir_path = os.getcwd()
		self.file_name = dir_path + "\\" + file_name + self.suffix

		if os.path.exists(self.file_name) == False:
			file = open(self.file_name,'x')

	def read_data(self):
		ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
		root = ntree.getroot()
		for i in root.findall('employee'):
			empid= i.find('empid').text
			empname = i.find('empname').text
			department = i.find('department').text
			print("EMPID: "+ empid)        
			print("EmpName: "+ empname)
			print("Department: "+ department)
		print("End of list")

	def read_data_by_id(self):
		empid1 =input("Enter the emp id you want to search for: ")
		ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
		root = ntree.getroot()

		#go through each record to find the record with the specified Email address
		for i in root.findall('employee'):
				empid2 = i.find('empid').text

				#is the rquested id the same as this records id?
				if empid2==empid1:
						#we have found a match get all the record field values
						empid = i.find('empid').text
						empname = i.find('empname').text
						department = i.find('department').text
						#print the field values
						print("EMPLOYEE RECORD ID: "+ empid)
						print("EMP ID: "+ empid)        
						print("Emp Name: "+ empname)
						print("Department: "+ department)

	def write_data(self):
		ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
		root = ntree.getroot()
			#add a record-but we would need to check for duplicates first
		#ask the user for the student email address to search for
		empid =input("Enter the Employee id of the employee you want to search for: ")

		def recordExists(empid):
			ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
			root = ntree.getroot()

			#set the default return value to false
			return_value=False
			#go through each record to find the one with the specified Email address
			for i in root.findall('employee'):
				empid1 = i.find('empid').text
				#is the requested id the same as this records id?
				if empid1==empid:
					#we have found a match set the return value to true
					return_value=True
			#finally return if the record was found to the caller        
			return return_value

		exists=recordExists(empid)

		if exists==False:
			#gt a new id
			# nid=newid()
			nid = uuid.uuid1()

			#get the field values from the user
			print("Create a record")
			empname=input("Employee Name:")
			department=input("department:")

			#create a contact element at root level
			# newrecord = ET.SubElement(root, "employee",id=str(nid))
			newrecord = ET.SubElement(root, "employee")
			#add the fields into out new record
			ET.SubElement(newrecord, "empid").text = str(nid)
			ET.SubElement(newrecord, "empname").text = empname
			ET.SubElement(newrecord, "department").text = department

			#finally save the update
			# ntree.write(file_check(file_path))    
			ntree.write(r'C:\Users\nsurisetti\Desktop\database\employees.xml')    

		else:
			print("Record already exists")

	def del_data_by_id(self):
		ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
		root = ntree.getroot()
		
		#get a variable for the record that you want to delete
		deleterecord=input("Enter the Employee id of the record you want to delete: ")

		#OK, now loop through all records looking for specified email address
		for i in root.findall('employee'):
			#get the email of the current record, 
			empid= i.find('empid').text
			#check is this the email we are looking for?
			if empid == deleterecord:
				#YES: remove the record from the xml tree
				root.remove(i)            
				#finally save the update
				# ntree.write(file_check(file_path))   
				ntree.write(r'C:\Users\nsurisetti\Desktop\database\employees.xml')

	def update_data(self):
		
		ntree = ET.parse(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
		root = ntree.getroot()

		empid1 =input("Enter the employee id of the employee to edit: ")

		#go through each record to find the record with the specified id
		for i in root.findall('employee'):
			empid2 = i.find('empid').text

			#test: is the rquested id the same as this records id?
			if empid2==empid1:
				#YES: we have found a match get all the record field values
				empid = i.find('empid').text
				empname = i.find('empname').text
				department = i.find('department').text

				#edit employee name
				empname = input("Please enter empname:(" + empname + ")") or empname
				i.find('empname').text= empname
				
				#edit department of employee
				department = input("Please enter department:(" + department + ")") or department
				i.find('department').text= department

				
				#save the update
				# ntree.write(file_check(file_path))
				ntree.write(r'C:\Users\nsurisetti\Desktop\database\employees.xml')
				#print the field values
				print("EMPLOYEE ID: "+ empid + " UPDATED")

	def Employee_menu(self):
			print("")
			print("___________E M P L O Y E E  D A T A B A S E___________")
			print("1 -Show details of all employees")
			print("2 -Search for an specified employee")
			print("3 -Create a new employee")
			print("4 -update the details of an employee")
			print("5 -Delete an employee")
			print("6 -Exit")
			user_input=input("Choose option 1-5 or '6' to exit: ")

			while user_input !="6":

				if user_input =="1":
					self.read_data()
					break

				if user_input =="2":
					self.read_data_by_id()
					break

				if user_input =="3":
					self.write_data()
					break

				if user_input =="4":
					self.update_data()
					break

				if user_input =="5":
					self.del_data_by_id()
					break