from newtest import MY_DB_FILE

def get_input():
  new_obj = None 
  while(1):
    user_input =input("Enter the name of the file on which you want to perform data operations :")
    try:
      new_obj = MY_DB_FILE(user_input)
      new_obj.Employee_menu()
      break
    except TypeError:
      print("Please enter a valid type for input")
    except ValueError:
      print("Entered input must be alphabetic with atleast length 1")
  return new_obj

get_input()
